import React from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    TouchableOpacityProps,
} from "react-native";

interface SkillCardProps extends TouchableOpacityProps {
    skill: string;
}

export function SkillCard(props: SkillCardProps) {
    const { skill, ...rest } = props;

    return (
        <TouchableOpacity style={styled.buttonSkill} {...rest}>
            <Text style={styled.textSkill}>{skill}</Text>
        </TouchableOpacity>
    );
}

const styled = StyleSheet.create({
    buttonSkill: {
        backgroundColor: "#1f1e25",
        padding: 15,
        borderRadius: 50,
        alignItems: "center",
        marginVertical: 10,
    },

    textSkill: {
        color: "#fff",

        fontSize: 18,
        fontWeight: "bold",
    },
});
