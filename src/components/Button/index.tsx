import React from "react";
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    TouchableOpacityProps,
} from "react-native";

interface ButtonProps extends TouchableOpacityProps {
    title: string;
}

export function Button({ title, ...rest }: ButtonProps) {
    return (
        <TouchableOpacity activeOpacity={0.8} style={styled.button} {...rest}>
            <Text style={styled.buttonText}>{title}</Text>
        </TouchableOpacity>
    );
}

const styled = StyleSheet.create({
    button: {
        backgroundColor: "#a370f7",
        padding: 15,
        borderRadius: 8,
        alignItems: "center",
        marginTop: 20,
    },

    buttonText: {
        color: "#fff",
    },
});
