import React, { useEffect, useState } from "react";
import { TextInput, TextInputProps, StyleSheet, Platform } from "react-native";

interface InputProps extends TextInputProps {}

export function Input(props: InputProps) {
    const { value, onChangeText, ...rest } = props;

    return (
        <TextInput
            style={styled.input}
            placeholder="New skill"
            placeholderTextColor="#555"
            value={value}
            onChangeText={onChangeText}
            {...rest}
        />
    );
}

const styled = StyleSheet.create({
    input: {
        backgroundColor: "#1f1e25",
        color: "#fff",
        fontSize: 18,
        padding: Platform.OS === "ios" ? 15 : 10,
        marginTop: 30,
        borderRadius: 8,
    },
});
