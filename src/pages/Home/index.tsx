import React, { useEffect, useState } from "react";
import {
    Text,
    StyleSheet,
    SafeAreaView,
    Platform,
    FlatList,
} from "react-native";
import { Button } from "../../components/Button";
import { Input } from "../../components/Input";
import { SkillCard } from "../../components/SkillCard";

interface SkillData {
    id: string;
    name: string;
}

export function Home() {
    const [newSkill, setnewSkill] = useState("");
    const [greeting, setGreeting] = useState("");
    const [mySkills, setMySkills] = useState<SkillData[]>([]);

    function handleRemoveSkill(id: string) {
        setMySkills((oldState) =>
            oldState.filter((skill) => {
                skill.id !== id;
            })
        );
    }

    useEffect(() => {
        const currentHour = new Date().getHours();

        if (currentHour < 12) {
            setGreeting("Good morning!");
        } else if (currentHour >= 12 && currentHour < 18) {
            setGreeting("Goof afternoon!");
        } else {
            setGreeting("Good night!");
        }
    }, []);

    return (
        <SafeAreaView style={styled.container}>
            <Text style={styled.title}>Welcome, Alisson</Text>
            <Text style={styled.greetings}>{greeting}</Text>

            <Input value={newSkill} onChangeText={setnewSkill} />

            <Button
                title="New"
                onPress={() => {
                    if (!newSkill) return;

                    const data = {
                        id: `${new Date().getTime()}`,
                        name: newSkill,
                    };

                    setnewSkill("");
                    setMySkills([...mySkills, data]);
                }}
            />

            <Text style={[styled.title, { marginVertical: 50 }]}>
                My skills
            </Text>

            <FlatList
                data={mySkills}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => (
                    <SkillCard
                        skill={item.name}
                        onPress={() => {
                            handleRemoveSkill(item.id);
                        }}
                    />
                )}
            />
        </SafeAreaView>
    );
}

const styled = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#121015",
        paddingVertical: 70,
        paddingHorizontal: 30,
    },

    title: {
        color: "#fff",
        fontSize: 24,
        fontWeight: "bold",
    },

    input: {
        backgroundColor: "#1f1e25",
        color: "#fff",
        fontSize: 18,
        padding: Platform.OS === "ios" ? 15 : 10,
        marginTop: 30,
        borderRadius: 8,
    },

    greetings: {
        color: "#fff",
    },
});
