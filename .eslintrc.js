module.exports = {
    root: true,
    extends: "@react-native-community",
    eslintSisableNextLine: false,
};
